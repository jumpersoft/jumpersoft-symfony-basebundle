<?php
namespace Jumpersoft\BaseBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Psr\Log\LoggerInterface;

/**
 * Description of JumpersoftMapArrayExtension
 *
 * @author Angel Vallejo
 */
class JumpersoftMapArrayExtension extends Extension
{

    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * Este método mapea un array y busca las llaves en la cadena enviada reemplazandolas con el valor del array
     * Muy util para llenar templates HTML u otros textos con cadenas reemplazables.
     */
    public function mapArrayToStringReplace($array, $string, $chrQuote)
    {
        try {
            if (isset($array) && isset($string)) {
                $tmpString = $string;
                foreach (array_keys($array) as $key) {
                    $tmpString = str_replace($chrQuote . $key . $chrQuote, $array[$key], $tmpString);
                }
                return $tmpString;
            } else {
                return $string;
            }
        } catch (Exception $ex) {
            return $string;
        }
    }

    /**
     * Separa los campos y valores enviados en $requestArray de la lista en $arrayKeys
     * y los regresa en un nuevo array, util para filtrar campos.
     */
    public function makeArrayFromPostInput($requestArray, $arrayKeys)
    {
        $array = array();
        try {
            foreach ($arrayKeys as $key => $campo) {
                if (isset($requestArray[$key])) {
                    if (!empty($requestArray[$key]) || $requestArray[$key] === "0") {
                        $array[$key] = is_string($requestArray[$key]) ? trim($requestArray[$key]) : $requestArray[$key];
                        $array[$key] = $array[$key] == "true" ? "1" : ($array[$key] == "false" ? "0" : $array[$key]);
                    } else {
                        $array[$key] = $requestArray[$key];
                    }
                }
            }
            return $array;
        } catch (Exception $ex) {
            return array();
        }
    }

    /**
     * Uitl para actualizar un array de otro array
     */
    public function updateArrayFromArray($post, &$array, $exceptions = [])
    {
        try {
            foreach ($post as $key => $valor) {
                if (isset($post[$key]) && !in_array($key, $exceptions)) {
                    $array[$key] = trim($valor);
                }
            }
        } catch (Exception $ex) {
            
        }
    }
}
