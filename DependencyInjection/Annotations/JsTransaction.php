<?php

namespace Jumpersoft\BaseBundle\DependencyInjection\Annotations;

/**
 * This annotation allow us to validate the current token previously generated.
 * Usefull to use in Controllers to prevent Cross Site request forgery (CSRF).
 *
 *
 * @author Angel
 * @Annotation
 */
class JsTransaction
{

    /**
     * @var string
     */
    //public $errorMessage;

    /**
     * @return string
     */
//    public function getErrorMessage() {
//        return $this->errorMessage;
//    }
}
