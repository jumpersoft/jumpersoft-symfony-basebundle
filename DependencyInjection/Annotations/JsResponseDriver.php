<?php

namespace Jumpersoft\BaseBundle\DependencyInjection\Annotations;

use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface; //Use essential kernel component
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use ReflectionClass;
use ReflectionException;
use ReflectionObject;
use ReflectionMethod;
use RuntimeException;
use ReflectionFunction;
use Symfony\Component\HttpFoundation\Request;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\CookieTokenExtractor;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;

/**
 * This Driver is part of TokenValidation annotation that allow us to validate the current token previously generated.
 * Usefull to use in Controllers to prevent Cross Site request forgery (CSRF).
 *
 * @author Angel
 */
class JsResponseDriver implements EventSubscriberInterface
{

    //Trx vars
    private $reader;
    private $em;
    private $resolver;
    private $isJsTransaction = false;
    private $isJsCsrf = false;
    private $hasException = false;
    //Csrf vars
    private $securityTokenKey;
    private $csrfProvider;
    //JWT Encoder
    private $jwtEncoder;
    private $jwtTokenTtl;
    private $renewJwtToken = true;
    private $enviroment = '';

    public function __construct(
        Reader $reader,
        CsrfTokenManagerInterface $csrfProvider,
        $securityTokenKey,
        EntityManagerInterface $em,
        ControllerResolverInterface $resolver,
        $jwtTokenTtl,
        $enviroment,
        JWTEncoderInterface $jwtEncoder
    ) {
        $this->reader = $reader; //get annotations reader
        $this->securityTokenKey = $securityTokenKey;
        $this->csrfProvider = $csrfProvider;
        $this->em = $em;
        $this->resolver = $resolver;
        $this->hasException = false;
        $this->jwtEncoder = $jwtEncoder;
        $this->jwtTokenTtl = $jwtTokenTtl;
        $this->enviroment = $enviroment;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => ['onKernelController', 1],
            KernelEvents::RESPONSE => ['onKernelResponse', 2],
            KernelEvents::EXCEPTION => ['onKernelException', 10],
        ];
    }

    /**
     * This event will fire during any controller call
     */
    public function onKernelController(ControllerEvent $event)
    {
        if (!is_array($controllers = $event->getController())) { //return if no controller
            return;
        }

        list($controller, $method) = $controllers;

        try {
            $controller = new ReflectionClass($controller); // get controller
        } catch (ReflectionException $e) {
            throw new RuntimeException('Failed to read annotation!');
        }

        //$this->handleClassAnnotation($controller);

        $this->handleMethodAnnotation($controller, $method, $event);
    }

    //    private function handleClassAnnotation(ReflectionClass $controller): void {
    //        $annotation = $this->reader->getClassAnnotation($controller, CsrfTokenValidation::class);
    //
    //        if ($annotation instanceof CsrfTokenValidation) {
    //            //code here for anotation class
    //        }
    //    }

    private function handleMethodAnnotation(ReflectionClass $controller, string $method, ControllerEvent $event): void
    {
        $method = $controller->getMethod($method);
        $annotation = $this->reader->getMethodAnnotation($method, JsResponse::class);
        if ($annotation instanceof JsResponse) {

            // START CSRF LOGIC - VALID TOKEN
            if ($annotation->getCsrf() == "true" && $this->enviroment != "test") {
                $request = $event->getRequest();
                $this->isJsCsrf = true;
                $csrftoken = new CsrfToken($this->securityTokenKey, $request->cookies->get("csrf"));
                if (!$this->csrfProvider->isTokenValid($csrftoken)) {
                    throw new AccessDeniedHttpException(); //if any throw 403
                }
            }

            // START TRANSACTION LOGIC - START TRANSACTION
            if ($annotation->getTransaction() == "true") {
                $this->isJsTransaction = true;
                $this->em->getConnection()->beginTransaction();
            }

            // CHECK IF JWT SHOULD NOT BE RENEWED, BY DEFAULT A TOKEN ALIVE IS ALLWAYS RENEWED
            if ($annotation->getRenewJwtToken() == "false") {
                $this->renewJwtToken = false;
            }
        }
    }

    public function onKernelResponse(ResponseEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();
        $date = new \DateTime(null, new \DateTimeZone('America/Mexico_City'));

        // END TRANSACTION LOGIC - COMMIT OR ROLLBACK TRANSACTIÓN
        if ($this->isJsTransaction) {
            if (!$this->hasException) {
                $this->em->flush();
                $this->em->getConnection()->commit();
            } else {
                $this->em->getConnection()->rollBack();
                $this->isJsCsrf = false;
            }
        }

        // END CSRF LOGIC - RENEW CSRF TOKEN
        if ($this->isJsCsrf) {
            $token = $this->csrfProvider->refreshToken($this->securityTokenKey);
            $response->headers->setCookie(new Cookie("csrf", $token, 0, '/', null, true));
        }

        // RENEW JWTOKEN
        if ($this->renewJwtToken) {
            $jwtToken = $request->headers->get('x-site-jumpersoft') == "store" ? "store-bearer" : "bearer";
            $extractor = new CookieTokenExtractor($jwtToken);
            if ($token = $extractor->extract($request)) {
                try {
                    $payload = $this->jwtEncoder->decode($token);
                    $days = $date->diff(new \DateTime("@" . $payload["exp"]))->format('%a');
                    $minutes = $days ? 24 * 60 * $days : 0;
                    $hours = $date->diff(new \DateTime("@" . $payload["exp"]))->format('%H');
                    $minutes = $hours ? $minutes + (60 * $hours) : $minutes;
                    $minutes += $date->diff(new \DateTime("@" . $payload["exp"]))->format('%i');
                    if ($minutes <= 40) {
                        $token = $this->jwtEncoder->encode([
                            'username' => $payload["username"],
                            'exp' => (new \DateTime('+' . $this->jwtTokenTtl . ' second'))->getTimestamp(), //Set expiration time data
                            'ip' => $request->getClientIp()
                        ]);
                        $response->headers->setCookie(new Cookie($jwtToken, $token, 0, '/', null, true));
                    }
                    $response->headers->set('ses-exp', $payload["exp"]);
                } catch (\Exception $ex) {
                }
            }
        }

        $response->headers->set('serv-date', $date->format("Y-m-d H:i:sP"));

        // CHECK SYSTEM PARAMETERS
        $serverStatus = $this->em->createQuery("select p from E:Parameters p where p.id = :id")->setParameter("id", "SERVER_STATUS")->getOneOrNullResult();
        $serverStatus = $serverStatus ? $serverStatus->getValue() : "online";
        $response->headers->set('serverStatus', $serverStatus);

        // ... modify the response object
    }

    public function onKernelException(RequestEvent $event)
    {
        if ($this->isJsTransaction) {
            $this->hasException = true;
        }
    }
}
