<?php

namespace Jumpersoft\BaseBundle\DependencyInjection\Annotations;

use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface; //Use essential kernel component
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use ReflectionClass;
use ReflectionException;
use ReflectionObject;
use ReflectionMethod;
use RuntimeException;
use ReflectionFunction;
use Symfony\Component\HttpFoundation\Request;

/**
 * This Driver is part of TokenValidation annotation that allow us to validate the current token previously generated.
 * Usefull to use in Controllers to prevent Cross Site request forgery (CSRF).
 *
 * @author Angel
 */
class JsTransactionDriver implements EventSubscriberInterface
{
    private $reader;
    private $em;
    private $resolver;
    private $isJsTransaction = false;
    private $hasException = false;

    //private $logger;

    public function __construct(Reader $reader, EntityManagerInterface $em, ControllerResolverInterface $resolver)
    {
        $this->reader = $reader; //get annotations reader
        $this->em = $em;
        $this->resolver = $resolver;
        $this->hasException = false;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => ['onKernelController', 1],
            KernelEvents::RESPONSE => ['onKernelResponse', 2],
            KernelEvents::EXCEPTION => ['onKernelException', 10],
        ];
    }

    /**
     * This event will fire during any controller call
     */
    public function onKernelController(ControllerEvent $event)
    {
        if (!is_array($controllers = $event->getController())) { //return if no controller
            return;
        }

        list($controller, $method) = $controllers;

        try {
            $controller = new ReflectionClass($controller); // get controller
        } catch (ReflectionException $e) {
            throw new RuntimeException('Failed to read annotation!');
        }

        //$this->handleClassAnnotation($controller);
        $this->handleMethodAnnotation($controller, $method, $event);
    }

//    private function handleClassAnnotation(ReflectionClass $controller): void {
//        $annotation = $this->reader->getClassAnnotation($controller, CsrfTokenValidation::class);
//
//        if ($annotation instanceof CsrfTokenValidation) {
//            //code here for anotation class
//        }
//    }

    private function handleMethodAnnotation(ReflectionClass $controller, string $method, ControllerEvent $event): void
    {
        $method = $controller->getMethod($method);
        $annotation = $this->reader->getMethodAnnotation($method, JsTransaction::class);
        if ($annotation instanceof JsTransaction) {
            $this->isJsTransaction = true;
            $this->em->getConnection()->beginTransaction();
//            $request = $event->getRequest();
//            $controller = $this->resolver->getController($request);
//            $newController = function() use ($request, $controller, $annotation) {
//                // -> PRE-EXECUTE
//                $arguments = $this->resolver->getArguments($request, $controller);
//                $this->em->getConnection()->beginTransaction();
//                try {
//                    $rs = call_user_func_array($controller, $arguments); //Call controller
//                    // -> POST-EXECUTE
//                    $this->em->flush();
//                    $this->em->getConnection()->commit();
//                } catch (\Exception $ex) {
//                    $this->em->getConnection()->rollBack();
//                    //$errorMessage = $annotation->getErrorMessage();
//                    throw $ex;
//                }
//                return $rs;
//            };
//            $event->setController($newController);
        }
    }

    public function onKernelResponse(ResponseEvent $event)
    {
        if ($this->isJsTransaction) {
            if (!$this->hasException) {
                $this->em->flush();
                $this->em->getConnection()->commit();
            } else {
                $this->em->getConnection()->rollBack();
            }
        }
        // ... modify the response object
    }

    public function onKernelException(RequestEvent $event)
    {
        if ($this->isJsTransaction) {
            $this->hasException = true;
        }
    }
}
