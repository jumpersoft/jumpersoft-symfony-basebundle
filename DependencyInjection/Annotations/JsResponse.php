<?php

namespace Jumpersoft\BaseBundle\DependencyInjection\Annotations;

/**
 * This annotation allow us to validate the current token previously generated.
 * Usefull to use in Controllers to prevent Cross Site request forgery (CSRF).
 *
 *
 * @author Angel
 * @Annotation
 */
class JsResponse
{

    /**
     * @var string
     */
    public $transaction;
    
    /**
     * @var string
     */
    public $csrf;
    
    /**
     * @var string
     */
    public $renewJwtToken;

    /**
     * @return string
     */
    public function getTransaction()
    {
        return $this->transaction;
    }
    
    /**
     * @return string
     */
    public function getCsrf()
    {
        return $this->csrf;
    }
    
    /**
     * @return string
     */
    public function getRenewJwtToken()
    {
        return $this->renewJwtToken;
    }
}
