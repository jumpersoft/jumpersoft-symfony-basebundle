<?php

namespace Jumpersoft\BaseBundle\DependencyInjection;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Twig\Environment;
use SendGrid;

/**
 * @author Angel Vallejo
 */
class JumpersoftMailerExtension
{
    private $mailer;
    private $emailNoReply;
    private $sendGridApiKey;

    public function __construct(MailerInterface $mailer, Environment $templating, $debug, $emailNoReply, $sendGridApiKey)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->emailNoReply = $emailNoReply;
        $this->sendGridApiKey = $sendGridApiKey;
    }

    public function send($from, $emailTo, $subject, $text = null, $template = null, $fieldsValues = null, $attachments = null, $retry = true)
    {
        $message = ($template ? new TemplatedEmail() : new Email())
            ->from($from)
            ->to($emailTo)
            ->subject($subject);

        $template ? $message->htmlTemplate($template)->context($fieldsValues) : $message->text($text);

        if ($attachments) {
            foreach ($attachments as $file) {
                $message->attachFromPath($file);
            }
        }

        try {
            $this->mailer->send($message);
        } catch (TransportExceptionInterface $e) {
            if ($retry) {
                $this->send($from, $emailTo, $subject, $template, $fieldsValues, false, $attachments);
            } else {
                return false;
            }
        }

        return true;
    }

    public function sendNoReply($emailTo, $subject, $text, $attachments = null, $retry = true)
    {
        return $this->send($this->emailNoReply, $emailTo, $subject, $text, null, null, $attachments, $retry);
    }

    public function sendNoReplyTpl($emailTo, $subject, $template, $fieldsValues, $attachments = null, $retry = true)
    {
        //remove email field since it is used as a reserved variable in twig
        $fieldsValues["auxEmail"] = $fieldsValues["email"] ?? "";
        unset($fieldsValues["email"]);
        return $this->send($this->emailNoReply, $emailTo, $subject, null, $template, $fieldsValues, $attachments, $retry);
    }

    /**
     * Note: More detail of all functions: https://github.com/sendgrid/sendgrid-php/blob/master/USE_CASES.md#kitchen-sink
     */
    public function sendGridMail($from, $to, $subject, $content = null, $templateId = null, $tagFields = [], $bcc = null, $metaData = null)
    {
        $email = new SendGrid\Mail\Mail();
        $sendgrid = new \SendGrid($this->sendGridApiKey);

        $email->setFrom($from);
        $email->setSubject($subject);
        $email->addTo($to);

        if ($content) {
            $email->addContent("text/html", $content);
        }

        if ($templateId) {
            $email->setTemplateId($templateId);
            $email->addDynamicTemplateDatas($tagFields);
        }

        if ($bcc) {
            if (is_array($bcc)) {
                foreach ($bcc as $bc) {
                    $email->addBcc($bc);
                }
            } else {
                $email->addTo($bcc);
            }
        }

        $metaData ? $email->addCustomArgs($metaData) : null;

        return $response = $sendgrid->send($email);
    }
}
