<?php

namespace Jumpersoft\BaseBundle\DependencyInjection;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelInterface;
use Twig\Environment;
use Psr\Log\LoggerInterface;

/**
 * Description of JumpertsoftExceptionListener
 *
 * @author Angel
 */
class JumpertsoftExceptionListener
{
    private $logger;
    private $requestStack;
    const EXCEPTION_CODES = [0, 8, 510];

    public function __construct(KernelInterface $kernel, RequestStack $requestStack, LoggerInterface $logger, Environment $templating, $debug)
    {
        $this->kernel = $kernel;
        $this->requestStack = $requestStack;
        $this->logger = $logger;
        $this->templating = $templating;
        $this->debug = $debug;
    }

    public function onKernelException(RequestEvent $event)
    {
        $exception = $event->getThrowable(); // You get the exception object from the received event
        $code = method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : $exception->getCode();
        $error = [
            'code' => 500,
            'originalCode' => $code, 
            'detail' => in_array($code, self::EXCEPTION_CODES) ? ($compDetail = explode("@@", $exception->getMessage()))[0] : $exception->getMessage(),
            'debug' => $this->debug
        ];

        if ($exception instanceof \Doctrine\ORM\NoResultException) {
            $error["msg"] = "Hubo un problema al procesar su solicitud, verifique con el administrador por favor";
        } elseif ($exception instanceof \Doctrine\ORM\Query\QueryException) {
            $error["msg"] = "Error en la consulta, verifique con el administrador por favor";
        } elseif ($exception instanceof \Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException) {
            $error["msg"] = "El registro se encuentra relacionado con otro registro, por favor revise con el administrador";
        } elseif ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            $error["msg"] = "El recurso que busca no existe";
        } elseif ($exception instanceof \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException) {
            $error["msg"] = "Acceso denegado";
        } elseif ($exception instanceof \Exception) {
            $error["msg"] = $error["detail"];
        } else {
            $error["msg"] = "Ocurrio un error con su consulta, verifique con el adminsitrador";
        }

        //DEBUG MESSAGES
        if ($this->debug) {
            $error["exception"] = \get_class($exception);
            $error["file"] = $exception->getFile();
            $error["line"] = $exception->getLine();
            $error["trace"] = json_encode($exception->getTrace());
            $error["detail"] .= isset($compDetail) && count($compDetail) > 1 ? " - " . $compDetail[1] : "";
            $this->logger->error($error["detail"]);
        }

        if ($this->requestStack->getCurrentRequest()->isXmlHttpRequest()) {
            $error['success'] = false;
            $response = new JsonResponse($error, $error["code"]);
            $event->setResponse($response);
        } else {
            //Mensaje general
            $message = $this->templating->render('@baseViews/error/error.html.twig', $error);
            $response = new Response();
            $response->setContent($message);
            // HttpExceptionInterface is a special type of exception that holds status code and header details
            if ($exception instanceof Symfony\Component\HttpKernel\Exception\HttpExceptionInterface) {
                $response->setStatusCode($exception->getStatusCode());
                $response->headers->replace($exception->getHeaders());
            } else {
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            // Send the modified response object to the event
            $event->setResponse($response);
        }
    }
}
