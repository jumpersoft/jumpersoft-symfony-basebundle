<?php

namespace Jumpersoft\BaseBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Doctrine\ORM\EntityManagerInterface;

class ExchangeRate
{
    private $result = ["value" => null, "date" => null, "dataFromDB" => false, "success" => false, "msg" => ""];
    private $container;
    private $url;
    private $params;
    private $api;
    private $em;
    private $debug;

    public function __construct(Container $container, EntityManagerInterface $em, $debug)
    {
        $this->container = $container;
        $this->params = $this->container->getParameter("jumpersoft.exchangerate");
        $this->params = $this->params[$this->api = $this->params["api"]];
        $this->url = $this->params["url"];
        $this->em = $em;
        $this->debug = $debug;
    }

    public function getExchangeRate($currencyId = "usd", $flush = false)
    {
        $currency = $this->em->createQuery("select c from E:Currency c where c.id = :id")->setParameter("id", $currencyId)->getOneOrNullResult();

        if ($this->isExchangeRateUpdated($currency)) {
            $this->result["dataFromDB"] = true;
            $this->result["date"] = $currency->getUpdateDate();
            $this->result["value"] = $currency->getExchangeRate();
        } else {
            switch ($this->api) {
                case "banxico":
                    $this->getBanxico();
                    break;
            }

            if ($this->result["success"]) {
                $currency->setExchangeRate($this->result["value"]);
                $currency->setUpdateDate($this->result["date"]);
                $this->em->persist($currency);
                $flush ? $this->em->flush() : null;
            } else {
                $this->result["dataFromDB"] = true;
                $this->result["date"] = $currency->getUpdateDate();
                $this->result["value"] = $currency->getExchangeRate();
            }
        }

        $this->result["date"] = $this->result["date"]->format("Y-m-d H:i:s");
        return $this->result;
    }

    private function getBanxico()
    {
        try {
            $response = file_get_contents($this->url);
            $data = json_decode($response, true);
            if (($value = $data['bmx']['series'][0]['datos'][0]['dato']) != null && is_numeric($value)) {
                $this->result["value"] = $value;
                $this->result["date"] = \DateTime::createFromFormat("d/m/Y H:i", $data['bmx']['series'][0]['datos'][0]['fecha'] . " 00:00");
                $this->result["success"] = true;
            }
        } catch (\Exception $e) {
            $this->result["msg"] = $e->getMessage();
        }
        return $this->result;
    }

    private function isExchangeRateUpdated($currency)
    {
        $date = new \DateTime(null, new \DateTimeZone('America/Mexico_City'));
        if ($currency->getUpdateDate()) {
            $interval = $currency->getUpdateDate()->diff($date);
            if ($date->format("N") >= 6) {
                return $date->format("N") == 6 ? $interval->format("%a") <= 2 : $interval->format("%a") <= 3;
            } else {
                return $interval->format("%a") <= 1;
            }
        } else {
            return false;
        }
    }
}
