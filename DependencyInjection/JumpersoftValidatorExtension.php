<?php
namespace Jumpersoft\BaseBundle\DependencyInjection;

/**
 * Description of Validator
 *
 * @author Angel
 */
class JumpersoftValidatorExtension
{

    private static $json = array('expval' => array(), 'rules' => array(), 'messages' => array(), 'altValidator' => array());

    public static function getValidatorArray($arrValidators)
    {
        $jsonArray = array('rules' => array(), 'messages' => array());
        try {
            if (!empty($arrValidators)) {
                foreach ($arrValidators as $campo => $tipo) {
                    if (array_key_exists('validator', $tipo)) {
                        //Creamos arreglo de rules
                        if (array_key_exists('rules', $tipo['validator'])) {
                            $jsonArray['rules'][$campo] = $tipo['validator']['rules'];
                            if (isset($tipo['validator']['expval'])) {
                                $jsonArray['rules'][$campo]["regex"] = $tipo['validator']['expval']["exp"];
                                unset($jsonArray['rules'][$campo][$campo . 'Val']);
                            }
                            if (isset($jsonArray['rules'][$campo]['remote'])) {
                                unset($jsonArray['rules'][$campo]['remote']);
                            }
                            if (isset($jsonArray['rules'][$campo]['maxlength'])) {
                                $jsonArray['rules'][$campo]['max'] = $jsonArray['rules'][$campo]['maxlength'];
                                unset($jsonArray['rules'][$campo]['maxlength']);
                            }
                            if (isset($jsonArray['rules'][$campo]['minlength'])) {
                                $jsonArray['rules'][$campo]['min'] = $jsonArray['rules'][$campo]['minlength'];
                                unset($jsonArray['rules'][$campo]['minlength']);
                            }
                            if (isset($jsonArray['rules'][$campo]['equalTo'])) {
                                $jsonArray['rules'][$campo]['confirmed'] = str_replace("#", "", $jsonArray['rules'][$campo]['equalTo']);
                                unset($jsonArray['rules'][$campo]['equalTo']);
                            }
                        }
                        //Creamos arreglo de messages
                        if (array_key_exists('messages', $tipo['validator'])) {
                            $jsonArray['messages'][$campo] = $tipo['validator']['messages'];
                            if (isset($jsonArray['messages'][$campo][$campo . 'Val'])) {
                                $jsonArray['messages'][$campo]['regex'] = $jsonArray['messages'][$campo][$campo . 'Val'];
                                unset($jsonArray['messages'][$campo][$campo . 'Val']);
                            }
                            if (isset($jsonArray['messages'][$campo]['remote'])) {
                                unset($jsonArray['messages'][$campo]['remote']);
                            }
                            if (isset($jsonArray['messages'][$campo]['maxlength'])) {
                                $jsonArray['messages'][$campo]['max'] = $jsonArray['messages'][$campo]['maxlength'];
                                unset($jsonArray['messages'][$campo]['maxlength']);
                            }
                            if (isset($jsonArray['messages'][$campo]['minlength'])) {
                                $jsonArray['messages'][$campo]['min'] = $jsonArray['messages'][$campo]['minlength'];
                                unset($jsonArray['messages'][$campo]['minlength']);
                            }
                            if (isset($jsonArray['messages'][$campo]['equalTo'])) {
                                $jsonArray['messages'][$campo]['confirmed'] = $jsonArray['messages'][$campo]['equalTo'];
                                unset($jsonArray['messages'][$campo]['equalTo']);
                            }
                        }
                    }
                }
            } else {
                throw new Exception();
            }
        } catch (Exception $ex) {
            return array();
        }
        return $jsonArray;
    }

    /** Note: it's important to always get the returned data to avoid replace other filed if setValuesToEntity is used */
    public static function proccessInputValues($input, $validator)
    {
        $output = [];

        try {
            //Fill values in
            foreach ($validator as $key => $campo) {
                // Set default values if has an empty input
                if (!empty($validator[$key]['value']) && (is_array($validator[$key]['value']) || $validator[$key]['value'] === "0" || $validator[$key]['value'] === false)) {
                    $input[$key] = $validator[$key]['value'];
                }
            }

            foreach ($validator as $key => $campo) {
                if (isset($input[$key])) {
                    if (!empty($input[$key]) || $input[$key] === "0") {
                        $output[$key] = is_string($input[$key]) ? trim($input[$key]) : $input[$key];
                        $output[$key] = $output[$key] == "true" ? "1" : ($output[$key] == "false" ? "0" : $output[$key]);
                    } else {
                        $output[$key] = $input[$key];
                    }
                }
            }
            return $output;
        } catch (Exception $ex) {
            return [];
        }
    }

    public static function validate($fieldsValidations, $values, $variableFieldValidation = [], $avoidFields = [], $throwException = true)
    {
        try {
            foreach (array_keys($fieldsValidations) as $field) {
                if (isset($fieldsValidations[$field])) {
                    if ((isset($fieldsValidations[$field]["notValidateInBackEnd"]) && $fieldsValidations[$field]["notValidateInBackEnd"] == true) || in_array($field, $avoidFields)) {
                        continue;
                    }
                    $validator = $fieldsValidations[isset($variableFieldValidation[$field]) ? $variableFieldValidation[$field] : $field]["validator"];

                    if(!empty($validator))
                    {
                        $values[$field] = $values[$field] ?? "";

                        // Required
                        if ($validator['rules']['required'] == true && (!isset($values[$field]) || (empty($values[$field]) && $values[$field] != "0"))) {
                            if ($throwException) {
                                throw new \Exception("Existe un error en el formulario@@Campo requerido: $field", 510);
                            } else {
                                return false;
                            }
                        }

                        // Equal to
                        if (!empty($validator['rules']['equalTo']) && ($values[$field] != $values[$validator['rules']['equalTo']])) {
                            if ($throwException) {
                                throw new \Exception("Existe un error en el formulario@@El valor del campo $field no es le mismo que el campo {$validator['rules']['equalTo']} ", 510);
                            } else {
                                return false;
                            }
                        }

                        // Regex
                        if (!is_array($values[$field] ?? "")) {
                            $valueDecoded = html_entity_decode($values[$field]); // DECODE HTML ENTITIES

                            if (!empty($validator['rules']["regex"])) {
                                if (!preg_match('/' . $validator['rules']['regex'] . '/', $valueDecoded)) {
                                    if ($throwException) {
                                        throw new \Exception("Existe un error en el formulario@@Valor incorrecto en campo: $field", 510);
                                    } else {
                                        return false;
                                    }
                                }
                            }
                        }

                        // Min and max values
                        if ((!empty($values[$field]) || $values[$field] == "0") && !empty($validator['rules']['min_value']) && (float)$values[$field] < (float)$validator["rules"]["min_value"]) {
                           throw new \Exception("El valor del campo $field ($values[$field]) es menor al mínimo requerido: {$validator['rules']['min_value']}"); 
                        }
                        if ((!empty($values[$field]) || $values[$field] == "0") && !empty($validator['rules']['max_value']) && (float)$values[$field] > (float)$validator["rules"]["max_value"]) {
                           throw new \Exception("El valor del campo $field ($values[$field]) es mayor al máximo requerido: {$validator['rules']['max_value']}"); 
                        }
                    }
                }
            }
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
}
