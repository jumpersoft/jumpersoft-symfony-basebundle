<?php

namespace Jumpersoft\BaseBundle\DependencyInjection;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Description of JumpersoftUtilExtension
 *
 * @author Angel Vallejo
 */
class JumpersoftUtilExtension
{
    private $em;
    private $projectDir;

    public function __construct(EntityManagerInterface $em, $projectDir)
    {
        $this->em = $em;
        $this->projectDir = $projectDir;
    }

    public function getUid($fullUuid = false, $numChars = null)
    {
        return substr(md5(uniqid(rand(), true)), 0, $fullUuid && $numChars == null ? 32 : ($numChars ? $numChars : 20));
    }

    public function getLocalDateTimeString($format = 'Y-m-d H:i:sP')
    {
        return (new \DateTime(null, new \DateTimeZone('America/Mexico_City')))->format($format);
    }

    public function getLocalDateTime()
    {
        return new \DateTime(null, new \DateTimeZone('America/Mexico_City'));
    }

    public function getDateTime($date, $format = 'Y-m-d H:i:s')
    {
        return isset($date) ? \DateTime::createFromFormat($format, $date) : null;
    }

    public function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /** Note: it's important to use always the data returned by proccessInputValues in order to set only values that came from validator and to not replace other important values */
    public function setValuesToEntity($entity, $fields, $avoided = [], $avoidToPurify = [])
    {
        if (!empty($fields)) {
            $config = \HTMLPurifier_Config::createDefault();
            $config->set('Cache.SerializerPath', $this->projectDir.'/var/cache');
            $htmlPurify = new \HTMLPurifier($config);

            $metadata = $this->em->getClassMetadata(get_class($entity));
            $entity_fields = $metadata->getFieldNames();
            foreach ($fields as $property => $value) {
                if (!$avoided || !in_array($property, $avoided)) {
                    $method = 'set' . ucfirst($property);
                    if (in_array(trim($property), $entity_fields, true)) {
                        $type = $metadata->getTypeOfField($property);
                        $value = empty($value) && $value !== "0" && $value != 0 && $value !== false && !is_array($value) ? null : $value;
                        if ($type == "string" || $type == "text") {
                            if(!empty($value) || $value == "0"){
                                $entity->$method(in_array($property, $avoidToPurify ?? []) ? $value : $htmlPurify->purify(trim($value)));
                            }else{
                                $entity->$method(null);
                            }
                        } elseif ($type == "datetime") {
                            $entity->$method(!empty($value) ? (gettype($value) == "object" ? $value : $this->getDateTime($value)) : null);
                        } elseif (stristr("integer|smallint|decimal", $type)) {
                            $entity->$method($value === "" ? null : $value);
                        } elseif ($type == "json" ) {
                            $entity->$method(empty($value) ? null : $value);
                        } else {
                            $entity->$method($value);
                        }
                    } else {
                        if(substr($property,-2) == "Id"){
                            $property = substr($property, 0, strlen($property) - 2);
                            $method = 'set' . ucfirst($property);
                        }
                        if ($metadata->hasAssociation($property)) {
                            $assocEntity = $metadata->getAssociationTargetClass($property);
                            $entity->$method(empty($value) ? null : (gettype($value) == "object" ? $value : $this->em->getReference($assocEntity, $value)));
                        }
                    }
                }
            }
        }
    }

    public function findOneInArray($array, $function)
    {
        $rows = array_filter($array, $function);
        return count($rows) > 0 ? reset($rows) : null;
    }

    public function sanitizeFilename($filename, $beautify = true)
    {
        // sanitize filename
        $filename = preg_replace(
            '~
        [<>:"/\\\|?*]|            # file system reserved https://en.wikipedia.org/wiki/Filename#Reserved_characters_and_words
        [\x00-\x1F]|             # control characters http://msdn.microsoft.com/en-us/library/windows/desktop/aa365247%28v=vs.85%29.aspx
        [\x7F\xA0\xAD]|          # non-printing characters DEL, NO-BREAK SPACE, SOFT HYPHEN
        [#\[\]@!$&\'()+,;=]|     # URI reserved https://tools.ietf.org/html/rfc3986#section-2.2
        [{}^\~`]                 # URL unsafe characters https://www.ietf.org/rfc/rfc1738.txt
        ~x',
            '-',
            $filename
        );
        // avoids ".", ".." or ".hiddenFiles"
        $filename = ltrim($filename, '.-');
        // optional beautification
        if ($beautify) {
            $filename = $this->beautifyFilename($filename);
        }
        // maximize filename length to 255 bytes http://serverfault.com/a/9548/44086
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $filename = mb_strcut(pathinfo($filename, PATHINFO_FILENAME), 0, 255 - ($ext ? strlen($ext) + 1 : 0), mb_detect_encoding($filename)) . ($ext ? '.' . $ext : '');
        return $filename;
    }

    public function beautifyFilename($filename)
    {
        // reduce consecutive characters
        $filename = preg_replace(array(
            // "file   name.zip" becomes "file-name.zip"
            '/ +/',
            // "file___name.zip" becomes "file-name.zip"
            '/_+/',
            // "file---name.zip" becomes "file-name.zip"
            '/-+/'
                ), '-', $filename);
        $filename = preg_replace(array(
            // "file--.--.-.--name.zip" becomes "file.name.zip"
            '/-*\.-*/',
            // "file...name..zip" becomes "file.name.zip"
            '/\.{2,}/'
                ), '.', $filename);
        // lowercase for windows/unix interoperability http://support.microsoft.com/kb/100625
        $filename = mb_strtolower($filename, mb_detect_encoding($filename));
        // ".file-name.-" becomes "file-name"
        $filename = trim($filename, '.-');
        return $filename;
    }

    public function reorderEntity($id, $oldIndex, $newIndex, $entity, $subQuery, $params){
        $dir = $oldIndex < $newIndex ? "down" : "up";
        $upDownQuery = $dir == "down" ? "((a.sequence >= :oldIndex and a.sequence <= :newIndex) or a.sequence is null)" : "((a.sequence >= :newIndex and a.sequence <= :oldIndex) or a.sequence is null)";
        $obj = $this->em->getRepository("Jumpersoft\EcommerceBundle\Entity\\$entity")->findOneById($id);
        $objSiblings = $this->em->createQuery("select a from E:$entity a where $upDownQuery and a.id <> :id $subQuery order by a.sequence asc")
            ->setParameters(array_merge(["oldIndex" => $oldIndex, "newIndex" => $newIndex, "id" => $id],$params))->getResult();
        foreach ($objSiblings as $key => $a) {
            if ($dir == "down") {
                $a->setSequence($oldIndex++);
                $this->em->persist($a);
                if ($key + 1 == count($objSiblings)) {
                    $obj->setSequence($oldIndex++);
                    $this->em->persist($obj);
                }
            } else {
                if ($key == 0) {
                    $obj->setSequence($newIndex++);
                    $this->em->persist($obj);
                }
                $a->setSequence($newIndex++);
                $this->em->persist($a);
            }
        }
    }

}
