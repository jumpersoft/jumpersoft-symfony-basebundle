Installation
============

Step 1: Download the Bundle
---------------------------

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer require jumpersoft/symfony-base-bundle
```

This command requires you to have Composer installed globally, as explained
in the [installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

Step 2: Enable the Bundle
-------------------------

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...

            new Jumpersoft\BaseBundle\JumpersoftBaseBundle(),
        );

        // ...
    }

    // ...
}
```

Step 3: Configure Bundle
-------------------------

In "app/config/parameters.yml" and "..parameters.yml.dist" set configuration with:

- Base parameters, put all parameters with values optional by each utility:

    # Secrete api captcha key (like google recaptcha)
    jumpersoft.recaptcha.secretCaptchaKey: ''

    # For authentication utility, e.g. 'MyBundleBundle:User' or 'JumpersoftEcommerceBundle:User'
    jumpersoft.entityUser: ''

    # Route to redirect if authentication failure
    jumpersoft.redirectToOnAuthenticationFailure: ''

    # Default store id to for use with JumpersoftEcommerceBundle
    jscommerce.defaultStoreId: ''    

    # Email to use for no reply notifications
    mailer_email_no_reply: 'info@jumpersoft.com'

    # Email to use for use with JumpersoftEcommerceBundle notifications
    mailer_email_intern_for_notifications: 'webadmin@jumpersoft.com'

- Service parameters, add this configuration in your services.yml main file:

    services:    
        ...
        _defaults:
            bind:
                $defaultStoreId: '%jscommerce.defaultStoreId%'
                $debug: '%kernel.debug%'

- Security paramaters, you can change with your name:
    
    jumpersoft.security.token_key: '' # csrf token
    jumpersoft.security.jwt_private_key_path: '' # ssh path private key path
    jumpersoft.security.jwt_public_key_path:  ''  # ssh path public key path
    jumpersoft.security.jwt_key_pass_phrase: '' # ssh key pass phrase
    jumpersoft.security.jwt_token_ttl: 86400 # jwt duration        

In "app/config/config.yml" set the next lexic jwt authentication configuration:

- Add configuration below to use for with the listener Exception:
    
    framework:
        ...
        templating:
            engines: ['twig']

- Optional, add configuration below to add a shorcut to access to entities in DQL

    doctrine:
    ...
        orm:
            ..
            mappings:
                MainEntities:
                    type: annotation
                    # Changue to your bundle name
                    dir: '%kernel.project_dir%/src/AppBundle/Entity'
                    prefix: AppBundle\Entity
                    alias: E

- Add Lexit JWT Athentication configurarion:

    lexik_jwt_authentication:
        private_key_path: %jumpersoft.security.jwt_private_key_path%
        public_key_path:  %jumpersoft.security.jwt_public_key_path%
        pass_phrase:      %jumpersoft.security.jwt_key_pass_phrase%
        token_ttl:        %jumpersoft.security.jwt_token_ttl%


Step 4: Use the Bundle
-------------------------

This are the topics related to this bundle:

1. Templates:
    - Controls: input controls like text, combo, radio and other inputs more complex related to jquery and bootstratp controls like select2, datetimepicker etc.
    - Forms: generic forms like user login, user signup, address, and others.

2. Classes:
    - JumpersoftBaseController: extend of Controller symfony class for multiple purpose.
    - JwtAuthenticator: class for authentication and authorization use "GuardAuthenticator".

3. Extensions:
    - JumpersoftMailerExtension: extend of Mailer for multiples purpose.
    - JumpersoftMapArrayExtension: functions to map arrays from json request, to validate agains class validators-regex and more.
    - JumpersoftReCaptchaExtension: functions to handle captcha functions.
    - JwtAuthenticator: Class to handle Json Web Token Authentication.

4. Annotations
    - CsrfTokenValidation: Annotation to handle CSRF functions in controllers.

5. Twig extensions:
    - JumpersoftBaseTwigExtension: generic functions to handle formated dates in "America/Mexico_City" timezone.

6. Doctrine extensions for MySQL:
    - String functions: FORMAT
    - DateTime functions: DATE_FORMAT

Step 5: Notes:
-------------------------

We recommend use the bundle "nelmio/cors-bundle": "~1.4" in order to take all advantages with json web tokens.