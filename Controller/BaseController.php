<?php

namespace Jumpersoft\BaseBundle\Controller;

use Jumpersoft\EcommerceBundle\Entity\UserLog;
use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftUtilExtension;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Description of BaseController:
 * Base class with useful functions to inherit to other controls.
 *
 * @author Angel Vallejo
 */
class BaseController extends AbstractController
{

    protected $em;
    protected $jsUtil;
    protected $authChecker;
    protected $params;
    protected $debug;
    //Main jsReturn array
    protected $jsReturn = array("success" => true);
    //General messages
    protected $msg = array(
        "errorGeneral" => "Hubo problemas para registrar su petici&oacute;n, por favor intentelo nuevamente, si el problema persiste, contacte al administrador del sitio, gracias.",
        "errorDB" => "Error al guardar en DB",
        "errorForm" => "Existe un error en los campos del formulario, por favor verifique los datos ingresados e intente nuevamente, si el problema persiste, contacte al administrador del sitio, gracias.",
        "queryOK" => "Información consultada con éxito.",
        "queryError" => "Error al consultar la información.",
        "updateOK" => "Información editada con éxito.",
        "updateError" => "Error al actualizar el registro.",
        "insertOK" => "Registro creado con éxito",
        "insertError" => "Error al guardar el registro",
        "deletedOK" => "Registro eliminado con éxito",
        "deletedOKn" => "Registros eliminados con éxito",
        "deletedError" => "Error al eliminar el registro",
        "errorToUploadFile" => "Hubo problemas subir el archivo, intente nuevamente por favor",
        "invalidStatus" => "El cambio de estatus solcitado no esta permitido para este registro"
    );

    public function __construct(EntityManagerInterface $em, JumpersoftUtilExtension $jsUtil, AuthorizationCheckerInterface $authChecker, ParameterBagInterface $params, $defaultStoreId, $debug)
    {
        $this->em = $em;
        $this->defaultStoreId = $defaultStoreId;
        $this->debug = $debug;
        $this->jsUtil = $jsUtil;
        $this->authChecker = $authChecker;
        $this->params = $params;
    }

    protected function getLogger()
    {
        return $this->get("logger");
    }


    /**
      START HEADERS FUNCTIONS
     */
    public function addCookies(&$objJsResponse, $cookies)
    {
        if ($cookies) {
            foreach ($cookies as $cookie) {
                $objJsResponse->headers->setCookie(new Cookie($cookie['id'], $cookie['val'], $cookie['exp'] ? $cookie['exp'] : null, '/', null, true));
            }
        }
    }

    public function removeCookies(&$objJsResponse, $cookies)
    {
        if ($cookies) {
            foreach ($cookies as $cookie) {
                $objJsResponse->headers->clearCookie($cookie);
            }
        }
    }

    /**
      START RESPONSE FUNCTIONS
     */
    protected function responseJson($status = 200, $altJson = null, $cookies = null, $removeCookies = null)
    {
        $objJsResponse = new JsonResponse(null, $status);
        $this->addCookies($objJsResponse, $cookies);
        $this->removeCookies($objJsResponse, $removeCookies);
        unset($this->jsReturn["filters"]);
        return $objJsResponse->setData($altJson ? $altJson : $this->jsReturn);
    }

    protected function responseFile($data, $type, $fileName)
    {
        return new Response($data, 200, ['Content-Type' => $type, 'Content-Disposition' => "attachment; filename=$fileName", "Access-Control-Expose-Headers" => 'Content-Disposition']);
    }

    protected function responseXls($data, $fileName)
    {
        return new StreamedResponse(
            function () use ($data) {
                $data->save('php://output');
            },
            200,
            ['Content-Type' => 'application/vnd.ms-excel', 'Content-Disposition' => 'attachment;filename="' . $fileName . '"', 'Cache-Control' => 'max-age=0', "Access-Control-Expose-Headers" => 'Content-Disposition']
        );
    }

    protected function getJson($array)
    {
        $objJsResponse = new JsonResponse();
        return $objJsResponse->setData($array);
    }

    protected function response($msg)
    {
        return new Response($msg, 200);
    }

    protected function responsErrorHTML()
    {
        return $this->render('@baseViews/test:test.html.twig', array('page_name' => 'Error', 'page_id' => 'error', 'text' => 'Error'));
    }
    /*
     * END RESPONSE FUNCTIONS
     */

    /**
     * START FILER DATATABLES, LIST AND EXTJS GRID FUNCTIONS
     */
    protected function getExtJsFilters($request)
    {
        $this->page = $request->get('page');
        $this->limit = $request->get('limit');
    }

    protected function getFilters($request, $decodeFilters = false)
    {
        $data = $request->getMethod() == "GET" ? $request->query->all() : $request->request->all();
        //Global filters
        $this->jsReturn['start'] = $data['start'] ?? "0";
        $this->jsReturn['length'] = isset($data['length']) ? ($data['length'] > 100 ? 100 : $data['length']) : "20";
        $this->jsReturn['filters'] = isset($data["filters"]) ? !$decodeFilters ? $data['filters'] : json_decode($data["filters"], true) : [];
        //Datatable filters
        $this->jsReturn['draw'] = $data['draw'] ?? "";
        //Jscommerce List filter
        $this->jsReturn['orderBy'] = $data['orderBy'] ?? "";
        $this->jsReturn['order'] = $data['order'] ?? "";
        $this->jsReturn['q'] = $data['q'] ?? "";
    }
    /*
     * END FILER DATATABLES, LIST AND EXTJS GRID FUNCTIONS
     */

    /**
     * START AUTHORIZATION FUNCTIONS
     */
    protected function isUser($role, $roles = [])
    {
        if (count($roles) == 0) {
            //Get result from currentuser
            return $this->authChecker->isGranted($role) ? true : false;
        } else {
            //Get result from passed array
            return in_array($role, $roles);
        }
    }
    /*
     * END AUTHORIZATION FUNCTIONS
     */

    /**
     * START GENERAL FUNCTIONS
     */
    protected function getTmpFileStoreDir($tmpDir)
    {
        $fs = new Filesystem();
        $tmpDir = 'tmp/files/stores/' . $this->jsUtil->getLocalDateTime()->format('Ym') . '/' . $tmpDir;
        if (!$fs->exists($tmpDir)) {
            $fs->mkdir($tmpDir);
        }
        return $tmpDir;
    }

    protected function getFileStoreDir($storeId)
    {
        $fs = new Filesystem();
        $storeDir = 'files/stores/' . $storeId;
        if (!$fs->exists($storeDir)) {
            $fs->mkdir($storeDir);
        }
        return $storeDir;
    }

    protected function getFileStoreItemDir($storeId, $itemId)
    {
        $fs = new Filesystem();
        $itemDir = 'files/stores/' . $storeId . '/items/' . $itemId;
        if (!$fs->exists($itemDir)) {
            $fs->mkdir($itemDir);
        }
        return $itemDir;
    }

    protected function getFileStoreCategoryDir($storeId, $categoryId)
    {
        $fs = new Filesystem();
        $categoryDir = 'files/stores/' . $storeId . '/categories/' . $categoryId;
        if (!$fs->exists($categoryDir)) {
            $fs->mkdir($categoryDir);
        }
        return $categoryDir;
    }

    protected function getFileStoreImagesDir($storeId)
    {
        $fs = new Filesystem();
        $dir = 'files/stores/' . $storeId . '/images';
        if (!$fs->exists($dir)) {
            $fs->mkdir($dir);
        }
        return $dir;
    }
    
    protected function getFileStoreViewLayoutDir($storeId, $storeViewLayoutId)
    {
        $fs = new Filesystem();
        $storeViewLayoutDir = 'files/stores/' . $storeId . '/layouts/' . $storeViewLayoutId;
        if (!$fs->exists($storeViewLayoutDir)) {
            $fs->mkdir($storeViewLayoutDir);
        }
        return $storeViewLayoutDir;
    }

    public function getDefaultStoreId($user)
    {
        if (gettype($user) == "object") {
            $store = $user->getDefaultStore();
            return $store ? $store->getId() : $this->defaultStoreId;
        } else {
            return $this->defaultStoreId;
        }
    }

    public function getEnviroment()
    {
        return $this->debug ? "dev" : "prod";
    }
    /*
     * END GENERAL FUNCTIONS
     */

    public function setNotRequiredFields(&$fieldsValidations, $arrayCampos)
    {
        foreach ($arrayCampos as $campo) {
            //$this->logg->info('CAMP XXX: ' . $campo);
            if (isset($fieldsValidations[$campo])) {
                if (array_key_exists('validator', $fieldsValidations[$campo])) {
                    if (array_key_exists('rules', $fieldsValidations[$campo]['validator'])) {
                        if (array_key_exists('required', $fieldsValidations[$campo]['validator']['rules'])) {
                            $fieldsValidations[$campo]['validator']['rules']['required'] = false;
                        }
                    }
                }
            }
        }
    }
    /*
     * END FORM FUNCTIONS
     */

    /*
     * START PROCESS FUNCTIONS
     */

    /**
     * fromStatusId can by string or array
     */
    public function validateChangeStatus($stepProcess, $fromStatusId, $toStatusId)
    {
        if (array_key_exists($toStatusId, $stepProcess)) {
            if (array_key_exists("from", $stepProcess[$toStatusId]) && in_array($fromStatusId, $stepProcess[$toStatusId]["from"])) {
                return true;
            } else if (array_key_exists("fromAnd", $stepProcess[$toStatusId])) {
                $status = !is_array($fromStatusId) ? [$fromStatusId] : $fromStatusId;
                foreach ($status as $fromStatusId) {
                    if (!in_array($fromStatusId, $stepProcess[$toStatusId]["fromAnd"])) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }
    /*
     * END PROCESS FUNCTIONS
     */
}
