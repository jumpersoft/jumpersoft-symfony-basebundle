<?php

namespace Jumpersoft\BaseBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpKernel\Config\FileLocator;
use Symfony\Component\Security\Core\User\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\BaseBundle\DependencyInjection;
use Psr\Log\LoggerInterface;

/**
 */
class TestController extends BaseController
{
    /**
     * @Route("/dev/mock-data/{id}", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function geMockDatatAction($id, FileLocator $fileLocator)
    {
        if ($this->debug) {
            $pathFile = $this->getParameter("mock-data-yml");
            $mockData = Yaml::parseFile($fileLocator->locate($pathFile));
            $this->jsReturn["data"] = $mockData[$id];
        }
        return $this->responseJson();
    }

    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/dev/get-xxx/{id}", methods={"GET"})
     */
    public function getAction($id, Request $request)
    {
        $this->jsReturn["data"] = $this->em->getReference("Jumpersoft\EcommerceBundle\Entity\Item", $id)->getParent();
        return $this->responseJson();
    }

    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/test/email/{orderRecordId}", methods={"GET"})
     * @JsResponse(transaction="true") 
     * NOTE: use only orders that bellow to user loged
     */
    public function testSendgrid($orderRecordId, DependencyInjection\JumpersoftMailerExtension $email, UserInterface $user = null)
    {
        if ($user) {
            if ($data = $this->em->getRepository('Jumpersoft\EcommerceBundle\Entity\OrderRecord')->getStoreOrderRecord($orderRecordId, $this->defaultStoreId, $user->getId())) {
                $email->sendNoReplyTpl($user->getEmail(), "Tu pedido No.{$data['folio']}", '@notificationViews/checkoutConfirmation.html.twig', $data);
                $this->jsReturn["msg"] = "correo enviado";
            } else {
                $this->jsReturn["msg"] = "El pedido no pertenece al vendedor logeado";
            }
        } else {
            $this->jsReturn["msg"] = "Debe estar logeado";
        }
        return $this->responseJson();
    }

    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/test/template-twig/{orderRecordId}", methods={"GET"})
     * @JsResponse(transaction="true") 
     */
    public function testTemplateTwig($orderRecordId, UserInterface $user)
    {
        $data = $this->em->getRepository('Jumpersoft\EcommerceBundle\Entity\OrderRecord')->getStoreOrderRecord($orderRecordId, $this->defaultStoreId, $user->getId());
        return $this->render('@notificationViews/checkoutConfirmation.html.twig', $data);
    }

    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/test/slack", methods={"GET"})
     */
    public function getSlackAction(LoggerInterface $storeErrorsLogger)
    {
        $storeErrorsLogger->info("Prueba de mensajería 👻");
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/test-vue3", methods={"GET"})
     */
    public function getTestAction()
    {
            $this->jsReturn["data"] = "HOla lo lograste";
            return $this->responseJson();
    }
}
