<?php

namespace Jumpersoft\BaseBundle\Twig;

use Twig\TwigFilter;
use Twig\TwigFunction;

class JumpersoftBaseTwigExtension extends \Twig\Extension\AbstractExtension
{
    protected $defaultStoreId;

    public function __construct($defaultStoreId)
    {
        $this->defaultStoreId = $defaultStoreId;
    }

    public function getFunctions()
    {
        return array(
            new TwigFunction('getServerLocalDateTime', array($this, 'getServerLocalDateTime')),
            new TwigFunction('getServerLocalDateTimeString', array($this, 'getServerLocalDateTimeString')),
            new TwigFunction('getInitials', array($this, 'getInitials'))
        );
    }

    public function getFilters()
    {
        return array(
            new TwigFilter('toSpanishDate', array($this, 'toSpanishDate')),
            new TwigFilter('decodeHtml', array($this, 'decodeHtml')),
            new TwigFilter('ellipsis', array($this, 'ellipsis'))
        );
    }

    /**
     * Returns an object with local time at America/Mexico_City
     * @return DateTime
     */
    public function getServerLocalDateTime()
    {
        return new \DateTime(null, new \DateTimeZone('America/Mexico_City'));
    }

    /**
     * Returns and string with local time at America/Mexico_City in "Y-m-d H:i:s" format
     * @return string
     */
    public function getServerLocalDateTimeString()
    {
        return (new \DateTime(null, new \DateTimeZone('America/Mexico_City')))->format('Y-m-d H:i:s');
    }

    /**
     * Returns initials of a name or phrase
     * @return string
     */
    public function getInitials($v)
    {
        $sp = explode(' ', $v);
        $r = '';
        if (count($sp) > 0) {
            foreach ($sp as $w) {
                $r .= $w[0];
            }
        }
        return $r;
    }

    /**
     */
    public function toSpanishDate($date, $isDay = false, $type = "short")
    {
        $dias = ["Sunday" => "Domingo", "Monday" => "Lunes", "Tuesday" => "Martes", "Wednesday" => "Miercoles", "Thursday" => "Jueves", "Friday" => "Viernes", "Saturday" => "Sábado"];
        $diasShort = ["Mon" => "Lun", "Tue" => "Mar", "Wed" => "Mie", "Thu" => "Jue", "Fri" => "Vie", "Sat" => "Sab", "Sun" => "Dom"];
        $meses = ["January" => "Enero", "February" => "Febrero", "March" => "Marzo", "April" => "Abril", "May" => "Mayo", "June" => "Junio", "July" => "Julio", "August" => "Agosto", "September" => "Septiembre", "October" => "Octubre", "November" => "Noviembre", "December" => "Diciembre"];
        $mesesShort = ["Jan" => "Ene", "Feb" => "Feb", "Mar" => "Mar", "Apr" => "Abr", "May" => "May", "Jun" => "Jun", "Jul" => "Jul", "Aug" => "Ago", "Sep" => "Sep", "Oct" => "Oct", "Nov" => "Nov", "Dec" => "Dic"];
        $days = "";
        if ($isDay) {
            if ($type == "short") {
                foreach ($diasShort as $key => $value) {
                    if (strpos($date, $key) !== false) {
                        return str_replace($key, $value, $date);
                    }
                }
            } else {
                foreach ($dias as $key => $value) {
                    if (strpos($date, $key) !== false) {
                        return str_replace($key, $value, $date);
                    }
                }
            }
        } else {
            if ($type == "short") {
                foreach ($mesesShort as $key => $value) {
                    if (strpos($date, $key) !== false) {
                        return str_replace($key, $value, $date);
                    }
                }
            } else {
                foreach ($meses as $key => $value) {
                    if (strpos($date, $key) !== false) {
                        return str_replace($key, $value, $date);
                    }
                }
            }
        }
        return $date;
    }

    /**
     */
    public function decodeHtml($v)
    {
        return html_entity_decode($v);
    }

    public function ellipsis($text, $maxLen = 50, $ellipsis = '...')
    {
        if (strlen($text) <= $maxLen) {
            return $text;
        }
        return preg_replace('/\s+?(\S+)?$/', '', substr($text, 0, $maxLen)) . $ellipsis;
    }
}
