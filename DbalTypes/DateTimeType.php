<?php
namespace Jumpersoft\BaseBundle\DbalTypes;

use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\DateTimeType as DbalDateTimeType;
use Doctrine\DBAL\Types\Type;

/**
 * Ref: https://www.easycharter.aero/software-development/database-column-type-datetime-with-microseconds/
 * NOTE, AFVR: Por default doctrine no puede guardar microsegundos, con este type sobreescribimos el type datatime para hacerlo,
 *             inicialmente no se necesitaba pero al hacer las pruebas para el registro de productos en el carrito los registros se hacian
 *             tan rápido que el valor de registerDate era el mismo para varios items lo que hacia que al traerlos en las consultas el orden
 *             a veces no era el correcto pues el valor de la fecha era el mismo, con esto se resuelve ese detalle, hay que notar que 
 *             mariadb o mysql en sus versiones mas recientes.
 *             Cuando se cree un nuevo campo de este tipo esta clase lo creará con el soporte para microsegundos, si el campo ya existe y es anterior a 
 *             la fecha de creación de este cambio si se debe hacer la migración para dicho campo, ver migración ejemplo Version20210822233235.
 */
final class DateTimeType extends DbalDateTimeType
{

    public function getName()
    {
        return Type::getType("datetime");
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        if (isset($fieldDeclaration['version']) && $fieldDeclaration['version'] == true) {
            return 'TIMESTAMP';
        }

        if ($platform instanceof PostgreSqlPlatform) {
            return 'TIMESTAMP(6) WITHOUT TIME ZONE';
        } else {
            return 'DATETIME(6)';
        }
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return $value;
        }

        if ($value instanceof DateTimeInterface) {
            return $value->format('Y-m-d H:i:s.u');
        }

        throw ConversionException::conversionFailedInvalidType($value, $this->getName(), ['null', 'DateTime']);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value instanceof DateTimeInterface) {
            return $value;
        }

        $val = DateTime::createFromFormat('Y-m-d H:i:s.u', $value);

        if (!$val) {
            $val = date_create($value);
        }

        if (!$val) {
            throw ConversionException::conversionFailedFormat($value, $this->getName(), $platform->getDateTimeFormatString());
        }

        return $val;
    }
}

