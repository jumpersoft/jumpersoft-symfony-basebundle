<?php

namespace Jumpersoft\BaseBundle\Security;

use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\CookieTokenExtractor;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

/**
 * Ref: http://symfony.com/doc/2.8/security/guard_authentication.html
 */
class JwtAuthenticator extends AbstractGuardAuthenticator
{
    private $em;
    private $jwtEncoder;
    private $redirectToOnAuthenticationFailure;

    public function __construct(EntityManagerInterface $em, JWTEncoderInterface $jwtEncoder, $redirectToOnAuthenticationFailure)
    {
        $this->em = $em;
        $this->jwtEncoder = $jwtEncoder;
        $this->redirectToOnAuthenticationFailure = !empty($redirectToOnAuthenticationFailure) ? $redirectToOnAuthenticationFailure : "/";
    }

    public function supports(Request $request)
    {
        return $this->getCredentials($request);
    }

    /**
     * Called on every request. Return whatever credentials you want,
     * or null to stop authentication.
     */
    public function getCredentials(Request $request)
    {
        $cookie = $request->headers->get('x-site-jumpersoft') == "store" ? "store-bearer" : "bearer";

        //A. FIRST METHOD: For Token stored in Cookies protected with http_only
        $extractor = new CookieTokenExtractor($cookie);

        $token = $extractor->extract($request);
        if (!$token) {

            //B. SECOND METHOD: For Token stored in Headers sent by AJAX - No used for Token in cookies
            if (!$request->headers->has('Authorization')) {
                return;
            }
            $extractor = new AuthorizationHeaderTokenExtractor('Bearer', 'Authorization');
            $token = $extractor->extract($request);
            if (!$token) {
                return; //Let authenticator decide if request is allowed to proced in anonymous moode or not
            }
        }

        try {
            // VALIDATE IF TOKEN HAS NOT EXPIRED, AFTER THIS WE CAN CHECK IF TOKEN OR USER HAS NOT IN BLACKLIST
            $payload = $this->jwtEncoder->decode($token);

            // VALIDATE IP
            if (!isset($payload['ip']) || $payload['ip'] !== $request->getClientIp()) {
                $event->markAsInvalid();
            }

            // VALIDATE AGAINS LAST TOKEN GENERATED


        } catch (\Exception $ex) {
            $var = $ex->getMessage();
            return;
        }

        return $token;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $data = $this->jwtEncoder->decode($credentials);
        if (!$data) {
            return;
        }
        $username = $data['username'];
        $user = $this->em->createQuery("select u from E:User u where u.username = :username")->setParameter("username", $username)->getOneOrNullResult();
        if(!$user){
            try{
                $user = $this->em->createQuery("select u from E:CustomerAuth u where u.username = :username")->setParameter("username", $username)->getOneOrNullResult();
            }catch(\Exception $ex){
                $user = null;
            }
        }

        if (!$user) {
            throw new AuthenticationCredentialsNotFoundException();
        }
        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        // check credentials - e.g. make sure the password is valid
        // no credential check is needed in this case
        // return true to cause authentication success
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($request->isXmlHttpRequest()) {
            $response = new JsonResponse(array('message' => $exception->getMessageKey()), 401);
        } else {
            $requestPath = $request->getPathInfo();
            $redirect = preg_match('[(/store/account)|(/store/checkout)]', strtolower($requestPath)) ? "/store/login?from=$requestPath" : "$this->redirectToOnAuthenticationFailure?from=$requestPath";
            $response = new RedirectResponse($redirect);
        }
        return $response;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continu
        return;
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse('Auth header required', 401);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
