<?php

declare(strict_types=1);

namespace Jumpersoft\BaseBundle\DoctrineExtensions;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * MigrationBase, don't use preUp en migration extend class in order to catch em and dbName
 */
class MigrationBase extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    public $em, $dbName;

    public function preUp(Schema $schema): void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->dbName = $this->em->getConnection()->getDatabase();
    }

    public function up(Schema $schema): void
    {
    }
}
