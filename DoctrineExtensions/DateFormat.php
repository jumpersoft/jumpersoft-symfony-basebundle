<?php

namespace Jumpersoft\BaseBundle\DoctrineExtensions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class DateFormat extends FunctionNode
{
    public $first_param;
    public $second_param;

    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER); //identifie la fonction DATE_FORMAT() de mysql
        $parser->match(Lexer::T_OPEN_PARENTHESIS); //parenthèse ouvrante
        $this->first_param = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA); // (5)
        $this->second_param = $parser->ArithmeticPrimary(); // (6)
        $parser->match(Lexer::T_CLOSE_PARENTHESIS); ////parenthèse fermante
    }

    public function getSql(SqlWalker $sqlWalker)
    {
        return 'DATE_FORMAT(' . $this->first_param->dispatch($sqlWalker) . ',' . $this->second_param->dispatch($sqlWalker) . ')';
    }
}
