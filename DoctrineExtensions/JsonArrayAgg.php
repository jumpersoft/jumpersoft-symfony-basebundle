<?php

namespace Jumpersoft\BaseBundle\DoctrineExtensions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Subselect;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class JsonArrayAgg extends FunctionNode
{
    public $subselect;


    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->expressions = $parser->StringExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }


    public function getSql(SqlWalker $sqlWalker)
    {
        return 'JSON_ARRAYAGG(' . $this->expressions->dispatch($sqlWalker) . ')';
    }
}

