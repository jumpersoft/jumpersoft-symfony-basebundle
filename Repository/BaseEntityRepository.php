<?php

namespace Jumpersoft\BaseBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * An entiry can extend directly from EntityRepository, but
 * in this way we can extend other util functions.
 *
 * @author Angel
 */
class BaseEntityRepository extends EntityRepository
{

    /**
     * getUID
     */
    public function getUID()
    {
        return md5(uniqid(rand(), true));
    }

    /**
     * concat
     * Build string with CONCAT sql function based in array
     */
    public function concat($array)
    {
        //$array = array("'Please'", "'PHP'", "'code'", "'tester'", "'my'");
        $res = "";
        $format = "CONCAT(%a, %b)";
        if (count($array) > 1) {
            $res = str_replace("%a", "COALESCE(" . $array[count($array) - 2] . ", '')", $format);
            $res = str_replace("%b", "COALESCE(" . $array[count($array) - 1] . ", '')", $res);
            if (count($array) > 2) {
                for ($i = count($array) - 3; $i >= 0; $i--) {
                    $tmp = str_replace("%a", "COALESCE(" . $array[$i] . ", '')", $format);
                    $res = str_replace("%b", $res, $tmp);
                }
            }
            return $res;
        } else {
            return "''";
        }
    }

    /**
     * setConditionalFilters
     * Build query with search filters
     * DEPRECADA - Use setFilters
     */
    public function setConditionalFilters(&$where, &$params, $filterValues, $fields)
    {
        $tmp = null;
        $op = "";
        if ($filterValues && isset($filterValues["filters"])) {
            foreach ($fields as $field => $filter) {
                //AND filters
                if (is_string($filter)) {
                    foreach ($filterValues["filters"] as $keyFilter => $data) {
                        if ($filter == $keyFilter && (strlen($data["value"] ?? "") > 0 || strlen($data["valueMin"] ?? "") > 0 || strlen($data["valueMax"] ?? "") > 0)) {
                            if ($data["type"] == "string") {
                                $tmp .= $op . $field . " LIKE :" . $filter;
                                $op = " and ";
                                $params[$filter] = '%' . $data["value"] . '%';
                            } elseif ($data["type"] == "stringEqual") {
                                $tmp .= $op . $field . " = :" . $filter;
                                $op = " and ";
                                $params[$filter] = $data["value"];
                            } elseif ($data["type"] == "select" || $data["type"] == "boolean") {
                                $tmp .= $op . $field . " = :" . $filter;
                                $op = " and ";
                                $params[$filter] = $data["value"];
                            } elseif ($data["type"] == "number") {
                                if (strlen($data["value"] ?? "") > 0) {
                                    $tmp .= $op . $field . " = :" . $filter;
                                    $op = " and ";
                                    $params[$filter] = $data["value"];
                                }
                                if (strlen($data["valueMin"] ?? "") > 0) {
                                    $tmp .= $op . $field . " < :" . $filter . "Min";
                                    $op = " and ";
                                    $params[$filter . "Min"] = $data["valueMin"];
                                }
                                if (strlen($data["valueMax"] ?? "") > 0) {
                                    $tmp .= $op . $field . " > :" . $filter . "Max";
                                    $op = " and ";
                                    $params[$filter . "Max"] = $data["valueMax"];
                                }
                            } elseif ($data["type"] == "date" || $data["type"] == "datetime") {
                                $field = $data["type"] == "date" ? "DATE_FORMAT($field, '%Y-%m-%d')" : $field;
                                if (strlen($data["value"] ?? "") > 0) {
                                    $tmp .= $op . $field . " = :" . $filter;
                                    $op = " and ";
                                    $params[$filter] = $data["value"];
                                }
                                if (strlen($data["valueMin"] ?? "") > 0) {
                                    $tmp .= $op . $field . " < :" . $filter . "Min";
                                    $op = " and ";
                                    $params[$filter . "Min"] = $data["valueMin"];
                                }
                                if (strlen($data["valueMax"] ?? "") > 0) {
                                    $tmp .= $op . $field . " > :" . $filter . "Max";
                                    $op = " and ";
                                    $params[$filter . "Max"] = $data["valueMax"];
                                }
                            }
                        }
                    }
                } else { //OR filters, array of filters to group in OR arrangement
                    $tmpOr = null;
                    $op = "";
                    foreach ($filter as $or_field => $or_filter) {
                        foreach ($filterValues["filters"] as $keyFilter => $data) {
                            //TODO falta agregar todos los tipos como en los filtros AND
                            if ($or_filter == $keyFilter && strlen($data["value"] ?? "") > 0) {
                                $tmpOr .= $op . $or_field . " LIKE :" . $or_filter;
                                $op = " or ";
                                $params[$or_filter] = '%' . $data["value"] . '%';
                            }
                        }
                    }
                    $tmp = !empty($tmpOr) ? $tmp . (!empty($tmp) ? ' and ' : '') . ('(' . $tmpOr . ')') : $tmp;
                }
            }
            if (!empty($where) && !empty($tmp)) {
                $where .= (strpos($where, "where") === false ? " where " : " and ") . $tmp;
            } else {
                $where .= ((empty($where) && !empty($tmp) ? " where " : "")) . $tmp;
            }
        }
    }

    public function setDateBetweenQuery(&$where, &$params, $field, $filterValues, $compFields, $format = 'd/m/Y h:i A', $setStartEndTime = false, $returnString = false)
    {
        try {
            if ($filterValues && (!empty($filterValues[$compFields[0]]) || !empty($filterValues[$compFields[1]]))) {
                $startDate = !empty($filterValues[$compFields[0]]) ? $filterValues[$compFields[0]] : null;
                $endDate = !empty($filterValues[$compFields[1]]) ? $filterValues[$compFields[1]] : null;
                $startDate = !empty($startDate) ? \DateTime::createFromFormat($format, $startDate) : null;
                $endDate = !empty($endDate) ? \DateTime::createFromFormat($format, $endDate) : null;
                $tmp = "";

                //Set init and end hours
                $startDate = $startDate && $setStartEndTime ? $startDate->setTime(00, 00) : $startDate;
                $endDate = $endDate && $setStartEndTime ? $endDate->setTime(23, 59) : $endDate;

                if ($returnString) {
                    $startDate = $startDate ? $startDate->format('Y-m-d H:i:s') : null;
                    $endDate = $endDate ? $endDate->format('Y-m-d H:i:s') : null;
                }

                if (!empty($startDate) && !empty($endDate)) {
                    $tmp = $field . " >= :" . $compFields[0] . " and " . $field . " <= :" . $compFields[1];
                    $params[$compFields[0]] = $startDate;
                    $params[$compFields[1]] = $endDate;
                } elseif (!empty($startDate)) {
                    $tmp = $field . " >= :" . $compFields[0];
                    $params[$compFields[0]] = $startDate;
                } elseif (!empty($endDate)) {
                    $tmp = $field . " <= :" . $compFields[1];
                    $params[$compFields[1]] = $endDate;
                }


                $where .= (!empty($where) && !empty($tmp) ? " and " : (empty($where) && !empty($tmp) ? " where " : "")) . $tmp;
            }
        } catch (Exception $ex) {
        }
    }

    public function setNumberRange(&$where, &$params, $field, $filterValues, $compFields)
    {
        try {
            if ($filterValues) {
                $numberA = $filterValues[$compFields[0]];
                $numberB = $filterValues[$compFields[1]];
                $tmp = "";

                if (!empty($numberA) && !empty($numberB)) {
                    $tmp = $field . " >= :" . $compFields[0] . " and " . $field . " <= :" . $compFields[1];
                    $params[$compFields[0]] = $numberA;
                    $params[$compFields[1]] = $numberB;
                } elseif (!empty($numberA)) {
                    $tmp = $field . " >= :" . $compFields[0];
                    $params[$compFields[0]] = $numberA;
                } elseif (!empty($numberB)) {
                    $tmp = $field . " <= :" . $compFields[1];
                    $params[$compFields[1]] = $numberB;
                }


                $where .= (!empty($where) && !empty($tmp) ? " and " : (empty($where) && !empty($tmp) ? " where " : "")) . $tmp;
            }
        } catch (Exception $ex) {
        }
    }

    /**
     * setOrderFilters
     * Build query with order filter
     * DEPRECADA - Use setFilters
     */
    public function setOrderFilters($filters, $fields)
    {
        $order = "";
        $coma = "";
        if (isset($filters["orderBy"])) {
            foreach ($fields as $key => $field) {
                if (is_string($filters["orderBy"])) {
                    //For one column
                    if ($filters["orderBy"] == $key) {
                        $order .= " $field " . ($filters["order"] == "desc" ? "desc" : "asc");
                    }
                } elseif (is_array($filters["orderBy"])) {
                    //For more than one column
                    foreach ($filters["orderBy"] as $oKey => $dir) {
                        if ($oKey === $key) {
                            $order .= " $coma $field " . ($dir == "asc" ? "asc" : "desc");
                            $coma =  ",";
                        }
                    }
                }
            }
        }
        return !empty($order) ? " order by $order" : "";
    }

    public function setFilters(&$where, &$params, $filters, $fields)
    {
        //WHERE FUNCTIONS
        $tmp = null;
        $op = "";
        if ($filters && isset($filters["filters"])) {
            foreach ($fields as $field => $filter) {
                //AND filters
                if (is_string($filter)) {
                    foreach ($filters["filters"] as $keyFilter => $data) {
                        if ($filter == $keyFilter && (strlen($data["value"] ?? "") > 0 || strlen($data["valueMin"] ?? "") > 0 || strlen($data["valueMax"] ?? "") > 0)) {
                            if ($data["type"] == "string") {
                                $tmp .= $op . $field . " LIKE :" . $filter;
                                $op = " and ";
                                $params[$filter] = '%' . $data["value"] . '%';
                            } elseif ($data["type"] == "stringEqual") {
                                $tmp .= $op . $field . " = :" . $filter;
                                $op = " and ";
                                $params[$filter] = $data["value"];
                            } elseif ($data["type"] == "select" || $data["type"] == "boolean") {
                                $tmp .= $op . $field . " = :" . $filter;
                                $op = " and ";
                                $params[$filter] = $data["value"];
                            } elseif ($data["type"] == "number") {
                                if (strlen($data["value"] ?? "") > 0) {
                                    $tmp .= $op . $field . " = :" . $filter;
                                    $op = " and ";
                                    $params[$filter] = $data["value"];
                                }
                                if (strlen($data["valueMin"] ?? "") > 0) {
                                    $tmp .= $op . $field . " < :" . $filter . "Min";
                                    $op = " and ";
                                    $params[$filter . "Min"] = $data["valueMin"];
                                }
                                if (strlen($data["valueMax"] ?? "") > 0) {
                                    $tmp .= $op . $field . " > :" . $filter . "Max";
                                    $op = " and ";
                                    $params[$filter . "Max"] = $data["valueMax"];
                                }
                            } elseif ($data["type"] == "date" || $data["type"] == "datetime") {
                                $field = $data["type"] == "date" ? "DATE_FORMAT($field, '%Y-%m-%d')" : $field;
                                if (strlen($data["value"] ?? "") > 0) {
                                    $tmp .= $op . $field . " = :" . $filter;
                                    $op = " and ";
                                    $params[$filter] = $data["value"];
                                }
                                if (strlen($data["valueMin"] ?? "") > 0) {
                                    $tmp .= $op . $field . " < :" . $filter . "Min";
                                    $op = " and ";
                                    $params[$filter . "Min"] = $data["valueMin"];
                                }
                                if (strlen($data["valueMax"] ?? "") > 0) {
                                    $tmp .= $op . $field . " > :" . $filter . "Max";
                                    $op = " and ";
                                    $params[$filter . "Max"] = $data["valueMax"];
                                }
                            }
                        }
                    }
                } else { //OR filters, array of filters to group in OR arrangement
                    $tmpOr = null;
                    $op = "";
                    foreach ($filter as $or_field => $or_filter) {
                        foreach ($filters["filters"] as $keyFilter => $data) {
                            //TODO falta agregar todos los tipos como en los filtros AND
                            if ($or_filter == $keyFilter && strlen($data["value"] ?? "") > 0) {
                                $tmpOr .= $op . $or_field . " LIKE :" . $or_filter;
                                $op = " or ";
                                $params[$or_filter] = '%' . $data["value"] . '%';
                            }
                        }
                    }
                    $tmp = !empty($tmpOr) ? $tmp . (!empty($tmp) ? ' and ' : '') . ('(' . $tmpOr . ')') : $tmp;
                }
            }
            if (!empty($where) && !empty($tmp)) {
                $where .= (strpos($where, "where") === false ? " where " : " and ") . $tmp;
            } else {
                $where .= ((empty($where) && !empty($tmp) ? " where " : "")) . $tmp;
            }
        }
        //ORDER FUNCTIONS
        $order = "";
        if (isset($filters["orderBy"])) {
            foreach ($fields as $field => $key) {
                if (is_string($field)) {
                    //For one column
                    if ($filters["orderBy"] == $key) {
                        $order .= " $field " . ($filters["order"] == "desc" ? "desc" : "asc");
                    }
                }
            }
        }
        $where .= !empty($order) ? " order by $order" : "";
    }

    /**
     * getLocalDateTimeString
     * @param type $format
     * @return date or string
     */
    protected function getLocalDateTimeString($format = null)
    {
        $date = new \DateTime(null, new \DateTimeZone('America/Mexico_City'));
        return $format ? $date->format($format) : $date;
    }

    protected function getResultPaginated($sql, $params, &$filters, $setOutputWalkers = false, $keyId = null)
    {
        $query = $this->getEntityManager()->createQuery($sql)->setParameters($params)->setFirstResult($filters['start'])->setMaxResults($filters["length"]);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers($setOutputWalkers);
        $filters["recordsFiltered"] = $filters["recordsTotal"] = count($paginator);
        $rows = $query->getArrayResult();
        return $keyId && count($rows) == 1 && $rows[0][$keyId] == null ? [] : $rows;
        //NOTES: $keyId is a temporal solution for queries who have clauseles like MAX that makes return a empty row OR with NULLS,
        //       this is becouse we can't use HAVING clausule with paginator to prevent this behavior,
        //       remove this when HAVING clause can be used with paginator or can be possible work with plain queries that can be cached by doctrine if this can be possible
    }

    protected function getResult($sql, $params = [])
    {
        return $this->getEntityManager()->createQuery($sql)->setParameters($params)->getResult();
    }

    protected function getSingleResult($sql, $params = [])
    {
        return $this->getEntityManager()->createQuery($sql)->setParameters($params)->getSingleResult();
    }

    protected function getOneOrNullResult($sql, $params = [], $hydrate = null)
    {
        return $this->getEntityManager()->createQuery($sql)->setParameters($params)->getOneOrNullResult($hydrate);
    }

    protected function getSingleScalarResult($sql, $params = [])
    {
        return $this->getEntityManager()->createQuery($sql)->setParameters($params)->getSingleScalarResult();
    }

    protected function getCount($sql, $params = [])
    {
        return $this->getEntityManager()->createQuery($sql)->setParameters($params)->getSingleScalarResult();
    }

    protected function getResultFromRawSql($sql, $params = [], $types = [])
    {
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->executeQuery($sql, $params, $types);
        return $stmt->fetchAllAssociative();
    }

    protected function getOneResultFromRawSql($sql, $params = [], $types = [])
    {
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->executeQuery($sql, $params, $types);
        return $stmt->fetchAssociative();
    }

    protected function getSingleScalarResultFromRawSql($sql, $params = [], $types = [])
    {
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->executeQuery($sql, $params, $types);
        return $stmt->fetchColumn();
    }
    
    protected function getResultPaginatedFromRawSql($sql, $params = [], &$filters, $types = [])
    {
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->executeQuery("select count(*) num from ($sql) t", $params, $types);
        $filters['recordsTotal'] = $stmt->fetch()["num"];
        $filters['start'] = $filters['start'] ? $filters['start'] : 0;
        $filters['length'] = $filters['length'] ? $filters['length'] : 50;
        $stmt = $conn->executeQuery($sql . " LIMIT " . $filters['start'] . "," . $filters['length'], $params, $types);
        return $stmt->fetchAllAssociative();
    }

    public function jsonDecode(&$rows, $fields, $useAsRow = false, $singleRow = false)
    {
        $rows = $singleRow ? [$rows] : $rows;
        foreach ($rows as $i => $row) {
            foreach ($fields as $fieldComp) {
                if (strpos($fieldComp, "->") === false) {
                    $field = $fieldComp;
                    $childs = null;
                } else {
                    $this->tmp = preg_split('/->/', $fieldComp);
                    $field = $this->tmp[0];
                    $childs = preg_split('/,/', $this->tmp[1]);
                }

                if (!$useAsRow) {
                    if (!empty($rows[$i]->{$field})) {
                        if (is_string($rows->{$field} = json_decode($rows->{$field}))) {
                            $rows[$i]->{$field} = json_decode($rows[$i]->{$field}); //Esta segunda decodificación es por si anteriormente estaba escapado el json con caracteres especiales
                        }
                        if ($childs && count($childs) > 0) {
                            foreach ($childs as $child) {
                                $this->jsonDecode($rows[$i]->{$field}, [$child], false, false);
                            }
                        }
                    } else {
                        $rows[$i]->{$field} = $rows[$i]->{$field} == null ? [] : $rows[$i]->{$field};
                    }
                } else {
                    if (!empty($rows[$i][$field])) {
                        if (!is_array($rows[$i][$field]) && is_string($rows[$i][$field] = json_decode($rows[$i][$field], true))) {
                            $rows[$i][$field] = json_decode($rows[$i][$field], true); //Esta segunda decodificación es por si anteriormente estaba escapado el json con caracteres especiales
                        }
                        if ($childs && count($childs) > 0) {
                            foreach ($childs as $child) {
                                if (!$this->isAssocArray($rows[$i][$field])) {
                                    foreach ($rows[$i][$field] as $j => $row2) {
                                        if (isset($rows[$i][$field][$j][$child]) && !is_array($rows[$i][$field][$j][$child]) && is_string($rows[$i][$field][$j][$child] = json_decode($rows[$i][$field][$j][$child], true))) {
                                            $rows[$i][$field][$j][$child] = json_decode($rows[$i][$field][$j][$child], true); //Esta segunda decodificación es por si anteriormente estaba escapado el json con caracteres especiales
                                        }
                                    }
                                } elseif (is_string($rows[$i][$field][$child] = json_decode($rows[$i][$field][$child], true))) {
                                    $rows[$i][$field][$child] = json_decode($rows[$i][$field][$child], true); //Esta segunda decodificación es por si anteriormente estaba escapado el json con caracteres especiales
                                }
                            }
                        }
                    } else {
                        $rows[$i][$field] = [];
                    }
                }
            }
        }
        return $singleRow ? $rows[0] : $rows;
    }

    public function isAssocArray(array $array)
    {
        return count(array_filter(array_keys($array), 'is_string')) > 0;
    }
}
