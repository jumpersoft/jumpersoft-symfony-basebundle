<?php

namespace Jumpersoft\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Utils functions for models
 * NOTE: No tratar de inyectar en esta clase nada pues no funcionará ya que esta excluido inyectar clases que se creen en la carpeta Entity y
 *       por cuestiones de seguridad no recomiendan inyectar nada en clases de Entidades.
 *
 */
class JumpersoftBaseModel
{
}
